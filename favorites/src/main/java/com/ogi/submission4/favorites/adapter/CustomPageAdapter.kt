package com.ogi.submission4.favorites.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.ogi.submission4.favorites.R
import com.ogi.submission4.favorites.fragments.MovieFavoriteFragment
import com.ogi.submission4.favorites.fragments.TvShowFavoriteFragment

class CustomPageAdapter(fm: FragmentManager, private var context: Context): FragmentStatePagerAdapter(fm) {
    private val pages = listOf(
        MovieFavoriteFragment(),
        TvShowFavoriteFragment()
    )
    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
       return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> context.getString(R.string.fav_movies_title)
            1 -> context.getString(R.string.fav_tv_shows_title)
            else -> context.getString(R.string.fav_movies_title)
        }
    }
}