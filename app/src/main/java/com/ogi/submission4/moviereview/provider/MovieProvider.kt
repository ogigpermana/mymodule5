package com.ogi.submission4.moviereview.provider

import android.content.ContentProvider
import android.content.ContentUris
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import com.ogi.submission4.moviereview.model.database.MovieReviewDB
import com.ogi.submission4.moviereview.model.movie.entity.MovieData

class MovieProvider: ContentProvider() {
    private val AUTHORITY = "com.ogi.submission4.moviereview"
    private val MOVIES = 1
    private val MOVIE_ITEM = 2
    private lateinit var database: MovieReviewDB
    private val MATCHER = UriMatcher(UriMatcher.NO_MATCH)

    private val uriMatcher = MATCHER.apply {
        addURI(AUTHORITY, "movies", MOVIES)
        addURI(AUTHORITY, "movies/#", MOVIE_ITEM)
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        when(MATCHER.match(uri)){
            MOVIES -> {
                if (context == null) return null
                val id = database.movieDao().insert(MovieData.fromContentValues(values))
                context?.contentResolver?.notifyChange(uri, null)
                return ContentUris.withAppendedId(uri, id)
            }
            MOVIE_ITEM -> throw IllegalArgumentException("Invalid URI, cannot insert with ID: " + uri)
            else -> throw IllegalArgumentException("Unknown URI: " + uri)
        }
    }

    override fun query(p0: Uri, p1: Array<String>?, p2: String?, p3: Array<String>?, p4: String?): Cursor? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun update(p0: Uri, p1: ContentValues?, p2: String?, p3: Array<String>?): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun delete(p0: Uri, p1: String?, p2: Array<String>?): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getType(p0: Uri): String? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}