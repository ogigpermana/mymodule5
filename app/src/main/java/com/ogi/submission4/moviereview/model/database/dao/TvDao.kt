package com.ogi.submission4.moviereview.model.database.dao

import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.database.Cursor
import com.ogi.submission4.moviereview.model.tv.entity.TvData

@Dao
interface TvDao {
    @get:Query("SELECT * FROM tvshows")
    val all: List<TvData>

    @Query("SELECT * FROM tvshows WHERE id = :id")
    fun getById(id: Int?): TvData

    @Insert(onConflict = REPLACE)
    fun insert(tvshow: TvData)

    @Delete
    fun delete(tvshow: TvData)

    @Query("DELETE FROM tvshows WHERE id = :id")
    fun deleteById(id: Long): Int

    @Query("SELECT * FROM tvshows")
    fun allFavorite(): Cursor

    @Query("SELECT * FROM tvshows WHERE id = :id")
    fun getFavoriteById(id: Long): Cursor

    @Update
    fun update(tvshow: TvData): Int
}