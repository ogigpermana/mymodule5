package com.ogi.submission4.moviereview.utils

object MovieReviewConst {
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val IMG_URL = "https://image.tmdb.org/t/p/w500"
    const val STATE = "state"
    const val LANG_STATE = "lang"
    const val MOVIE_STATE = "movie_state"
    const val TVSHOW_STATE = "tv_state"
    const val FLAG_MOVIE = "movie"
    const val FLAG_TV = "tv"
}