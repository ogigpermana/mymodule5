package com.ogi.submission4.moviereview.model.movie

import com.ogi.submission4.moviereview.model.movie.entity.MovieData

class MovieResponse (
    val results: List<MovieData>
)