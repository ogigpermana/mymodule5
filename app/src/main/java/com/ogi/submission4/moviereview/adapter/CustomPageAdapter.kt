package com.ogi.submission4.moviereview.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.ogi.submission4.moviereview.R
import com.ogi.submission4.moviereview.view.fragments.movie.MovieFavoriteFragment
import com.ogi.submission4.moviereview.view.fragments.tv.TvShowFavoriteFragment

class CustomPageAdapter(fm: FragmentManager, private var context: Context): FragmentStatePagerAdapter(fm) {
    private val pages = listOf(
        MovieFavoriteFragment(),
        TvShowFavoriteFragment()
    )

    override fun getCount(): Int {
        return pages.size
    }

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position) {
            0 -> context.getString(R.string.menu_movies)
            1 -> context.getString(R.string.menu_tv_shows)
            else -> context.getString(R.string.menu_movies)
        }
    }
}